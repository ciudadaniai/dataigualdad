Rails.application.routes.draw do
  get 'data/indicator'
  get 'data/dashboard'
  get 'data/questions'
  get 'data/lolcat'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "data#questions"
  get 'about' => 'pages#show', page: 'about'
  get 'contact' => 'pages#show', page: 'contact'
  post 'contact' => 'pages#contact'
  # get "/:page" => "pages#show"
  get "/fact/:fact_id" => "data#fact"

end
