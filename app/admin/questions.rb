ActiveAdmin.register Question do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters

  permit_params :text,
                :concept,
                :measure,
                :visible,
                :background,
                :question_category_id,
                :question_phrase_id,
                :template,
                facts_attributes: [:id,
                                   :factable_id,
                                   :factable_type,
                                   :country_id,
                                   :year,
                                   :value,
                                   :_destroy]

  index do
    column :text, sortable: true
    column :concept, sortable: true
    column :question_category, sortable: true
    # column :question_phrase, sortable: true
    column :question_phrase, sortable: true do |question|
      question.question_phrase.phrase if question.question_phrase
    end
    # column :background, sortable: false do |question|
    #   image_tag question.background if question.background.attached?
    # end
    column :facts_count, sortable: false do |question|
      question.facts.count
    end
    actions
  end

  form title: 'Question Form' do |f|
    f.semantic_errors
    inputs 'Details' do
      input :question_category
      input :concept
      input :text, label:"'_value_' and '_country_' will be replaced at presentation. Use ** to highlight text"
      input :question_phrase, :collection => QuestionPhrase.all.map{|p| [p.phrase, p.id] }
      input :template
      # input :measure
      input :visible, label:"Visible"
      # input :background, :as => :file
    end
    f.actions
    # inputs 'Background Image' do
    #   image_tag(f.object.background) if f.object.background.attached?
    # end
    inputs 'Facts' do
      has_many :facts, heading: false, allow_destroy: true do |a|
        a.input :factable_id, input_html: {value: f.object.id}, as: :hidden
        a.input :factable_type, input_html: {value: "Question"}, as: :hidden
        a.input :country
        a.input :year
        a.input :value
      end
    end
    f.actions
  end

end
