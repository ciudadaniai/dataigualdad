ActiveAdmin.register Indicator do
  # belongs_to :indicator_category

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name, :description, :measure, :visible, :order, :indicator_category_id, :icon,
                facts_attributes: [:id,
                                   :factable_id,
                                   :factable_type,
                                   :country_id,
                                   :year,
                                   :value,
                                   :_destroy]
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :description, :measure, :visible, :order, :indicator_category_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  index do
    column :name, sortable: true
    column :description, sortable: true
    column :icon, sortable: false do |indicator|
      image_tag indicator.icon if indicator.icon.attached?
    end
    column :category
    column :measure
    column :visible
    column :order
    column :facts_count, sortable: false do |indicator|
      indicator.facts.count
    end

    actions
  end


  form title: 'Indicator Form' do |f|
    # f.render partial: 'form' unless f.object.new_record?
    f.semantic_errors
    inputs 'Details' do
      input :indicator_category
      input :name
      input :description
      input :icon, :as => :file
      input :measure
      input :visible
      input :order
      f.actions
      # input :created_at, label: "IndicatorCategory Published At"
      # li "Created at #{f.object.created_at}" unless f.object.new_record?
    end
    inputs 'Icon' do
      image_tag(f.object.icon) if f.object.icon.attached?
      f.actions
    end
    inputs 'Grid' do
      # form partial: 'form'
      # render partial: 'form', locals: { @indicator: f }
      f.render partial: 'form' unless f.object.new_record?
    end
    # panel 'Facts' do
    #   has_many :facts, heading: false, allow_destroy: true do |a|
    #     a.input :factable_id, input_html: {value: f.object.id}, as: :hidden
    #     a.input :factable_type, input_html: {value: "Indicator"}, as: :hidden
    #     a.input :country
    #     a.input :year
    #     a.input :value
    #   end
    # end
    # f.actions
  end

end
