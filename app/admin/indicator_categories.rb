ActiveAdmin.register IndicatorCategory do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name, :description, :icon,
                indicators_attributes: [:id, :description, :name, :icon, :_destroy]
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :description]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  # form partial: 'form'

  index do
    column :name, sortable: true
    column :description, sortable: true
    column :icon, sortable: false do |cat|
      image_tag cat.icon if cat.icon.attached?
    end

    actions
  end


  show do
    attributes_table do
      row :name
      row :description
      row :icon do |i|
        image_tag url_for(i.icon), class: 'category_icon_form_image' if i.icon.attached?
      end
    end
  end


  form title: 'Indicator Category Form' do |f|
    f.semantic_errors
    inputs 'Details' do
      input :name
      input :description
      input :icon, :as => :file
      # input :created_at, label: "IndicatorCategory Published At"
      # li "Created at #{f.object.created_at}" unless f.object.new_record?
    end
    inputs 'Icon' do
      image_tag(f.object.icon) if f.object.icon.attached?
    end
    panel 'Indicators' do
      # tabs do
      #   # start of table tab
      #   tab 'Table' do
          table_for f.object.indicators do |i|
            column :name
            column :description
            column :measure
            column :visible
            column :order
            column :icon, sortable: false do |indi|
              image_tag indi.icon if indi.icon.attached?
            end
            column 'action', :id, sortable: false do |i|
              link_to 'edit',
              edit_admin_indicator_path(i),
              method: :get
            end
          end
        # end
        # end of table tab
        # start of form tab
                  # tab 'Forms' do
                  #   f.has_many :indicators, allow_destroy: true do |t|
                  #     t.input :name
                  #     t.input :description
                  #     panel 'Icon' do
                  #       image_tag(t.object.icon) if t.object.icon.attached?
                  #     end
                  #     t.input :icon, :as => :file
                  #     t.input :measure
                  #     t.input :visible, :label => "visible"
                  #     t.input :order
                  #   end
                  # end
        # end of form tab
      # end
    end
    # inputs 'Content', :body
    # para "Press cancel to return to the list without saving."
    f.actions
  end

end
