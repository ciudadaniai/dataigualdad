ActiveAdmin.register Note do
  # belongs_to :indicator_category

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :text, :indicator_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :description, :measure, :visible, :order, :indicator_category_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  index do
    column :text, sortable: true
    column :indicator, sortable: true
    actions
  end

end
