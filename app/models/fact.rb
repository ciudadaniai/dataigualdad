class Fact < ApplicationRecord
  belongs_to :factable, polymorphic: true
  belongs_to :country

  has_one_attached :slide

  before_save :make_one_png

  def make_one_png
    if self.year_changed? or self.value_changed?
      unless self.question_text == ""
        logger.debug "Found changes in Fact with ID #{self.id}. Will update PNG!"
        self.make_and_attach_png
      end
    end
  end

  def question_text
    logger.debug "Called method Question Text"

    if self.factable_type == "Question"

      logger.debug "Factable type was Question"

      pre_text = self.factable.text

      value_text = ActiveSupport::NumberHelper.number_to_delimited(self.value, :delimiter => '.')

      value_text = value_text.gsub(/(\d+)\.0\z/, '\1')

      pre_text = pre_text.gsub '_value_', value_text
      post_text = pre_text.gsub '_country_', self.country.short_name

      logger.debug "Created a question text: #{post_text}"

      post_text
    else
      logger.debug "Factable type was not Question"

      ""
    end
  end

  def self.is_phrase_an_appropriate_length phrase
    lines = split_into_lines(phrase, 32)
    logger.debug lines
    lines.length <= 8
  end

  def make_svg
    # For svg image manipulation
    require 'victor'
    # For taking hash/digests of this fact
    require 'digest/md5'

    logger.debug "Making an SVG"

    question_text = self.question_text

    # We eliminate decimals if they're only zeroes
    fact_lines = self.class.split_into_lines(question_text, 23)

    font_number_at_23_per_line = 94

    current_char_number = 23

    current_font_number = font_number_at_23_per_line

    while fact_lines.length > 6 and current_char_number < 32
      current_char_number += 1
      current_font_number = font_number_at_23_per_line * 23 / current_char_number
      fact_lines = self.class.split_into_lines(question_text, current_char_number)
    end

    logger.debug "Fact lines: #{fact_lines}"
    logger.debug "Chars per line: #{current_char_number}"
    logger.debug "Font size: #{current_font_number}"

    # Classes:
      # <ID>: <class_of_normal_font>, <class_of_colored_font>
      # 0: K N, K
      # 1: L M, L
      # 2: L M, L
      # 3: K M, K L
      # 4: L N, I L
      # 5: L N, I L
      # 6: K M, K L
      # 7: M P, L M
    class_hashmap = { 0 => ['K N', 'K']}
    class_hashmap[1] = ['L M', 'L']
    class_hashmap[2] = ['L M', 'L']
    class_hashmap[3] = ['K M', 'K L']
    class_hashmap[4] = ['L N', 'I L']
    class_hashmap[5] = ['L N', 'I L']
    class_hashmap[6] = ['K M', 'K L']
    class_hashmap[7] = ['M P', 'L M']
    class_hashmap[8] = ['K M', 'K L']
    class_hashmap[9] = ['M P', 'L M']
    class_hashmap[10] = ['M P', 'L M']
    class_hashmap[11] = ['M P', 'L M']
    class_hashmap[12] = ['M P', 'L M']

    logger.debug "Created hashmaps and seeds"

    a = "#{self.country.short_name}"

    template_id = self.factable.template_before_type_cast
    logger.debug self.factable.template_before_type_cast

    if template_id == -1
      logger.info "Found no template for Fact of ID #{self.id}. Will use default template (#0)"
      template_id = 0
    end

    svg = Victor::SVG.new template: "app/assets/svg_templates/#{template_id}.svg", viewBox: '0 0 10 10'

    logger.debug "Started SVG from template"

    class_normal = class_hashmap[template_id][0]
    class_colored = class_hashmap[template_id][1]
    phrase_lines = self.class.split_into_lines(self.factable.question_phrase.phrase, 30)

    inner_svg = Victor::SVG.new viewBox: '0 0 100 100'

    delta_y = 94 - current_font_number

    initial_delta_y_factor = fact_lines.length - 6

    if initial_delta_y_factor < 0
      initial_delta_y_factor = 0
    end

    # This is only different from 0 if we have a Fact with more than 6 lines
    initial_delta_y = initial_delta_y_factor * delta_y

    current_y = 223.5 - 2 * delta_y - initial_delta_y

    logger.debug "Started inner SVG"

    inner_svg.build do
      text 'xml:space' => "preserve", font_size: "#{current_font_number}", letter_spacing: "0em", class: "#{class_normal}", text_anchor: "start"  do

        highlighting_fact = false

        tspan '', x: "110", y: "#{current_y}", font_weight: "600"

        fact_lines.each do |fact_line|

          current_y += 112 - delta_y

          tspan '', x: "110", y: "#{current_y}", font_weight: "600"

          fact_line.split(' ').each do |word|

            if word.start_with?('*')
              highlighting_fact = !highlighting_fact
            end

            printed_word = word.gsub('*', '')

            if highlighting_fact
              tspan printed_word, y: "#{current_y}", font_weight: "600", fill: "#665eff", class: "#{class_colored}"
            else
              tspan printed_word, y: "#{current_y}", font_weight: "600"
            end

            if word.end_with?('*')
              highlighting_fact = !highlighting_fact
            end
          end
        end

        current_y += 108

        tspan '', x: "110", y: "#{current_y}", font_size: "67"

        highlighting_phrase = false

        phrase_lines.each do |phrase_line|

          current_y += 93

          # Start of the line
          tspan '', x: "110", y: "#{current_y}", font_size: "67"

          phrase_line.split(' ').each do |word|
            if word.start_with?('*')
              highlighting_phrase = !highlighting_phrase
            end

            # We have to eliminate highlighting marks
            printed_word = word.gsub('*', '')

            if highlighting_phrase
              # We use a different font weight because highlights in the phrase are semibold (weight 600)
              tspan printed_word, y: "#{current_y}", font_size: "67", font_weight: "600", fill: "#665eff", class: "#{class_colored}"
            else
              tspan printed_word, y: "#{current_y}", font_size: "67"
            end

            if word.end_with?('*')
              highlighting_phrase = !highlighting_phrase
            end
          end
        end

        current_y += 155

        tspan '', x: "110", y: "#{current_y}", font_size: "52"

        tspan '#DataIgualdad #DatosParaActuar', y: "#{current_y}", font_size: "52", font_weight: "bold"

        current_y += 62

        tspan '', x: "110", y: "#{current_y}", font_size: "52"

        tspan "##{a}", y: "#{current_y}", font_size: "52", font_weight: "bold"
      end
    end

    logger.debug "Finished inner SVG"

    svg << inner_svg

    logger.debug "Inserted inner SVG into main SVG"

    # We return our svg
    svg
  end

  def make_and_attach_png
    require 'rmagick'

    logger.debug "Trying to make a PNG"

    q_t = self.question_text

    unless q_t == ""

      # SVG source
      svg = self.make_svg

      logger.debug "Made base SVG"

      # SVG blob
      svg_string = svg.render

      logger.debug "Rendered SVG"


      # We use the blob to avoid making an svg file
      png, data = Magick::Image.from_blob(svg_string) {
        self.format = 'SVG'
      }

      logger.debug "Made PNG from SVG blob"

      # FIXME: after we're done with this feature, delete this test string.
      # png.write "test_png_from_fact_#{self.id}_at_attachment_site.png"

      # logger.debug "Wrote debug PNG at 'test_png_from_fact_#{self.id}_at_attachment_site.png'"

      # We use a unique UUID so that we don't clash with other TempFiles
      filename = "#{SecureRandom.uuid}.png"

      directory = 'temp_imgs/'

      url = "#{directory}#{filename}"

      logger.debug "Created new tempfile"

      png.write url

      logger.debug "Wrote onto Tempfile"

      self.slide.attach(io: File.open(url), filename: filename, content_type: "image/png")

      logger.debug "Attached new PNG to Fact"
    end
  end

  def test_making_own_svg
    svg = self.make_svg

    svg.save "test_of_templating_from_fact_#{self.id}"
  end

  def path
    "/fact/#{self.id}"
  end

  private

  # Currently we're splitting at a constant maximum number of characters
  def self.split_into_lines(phrase, char_limit)
    words = phrase.split(' ')

    lines = []

    limit = char_limit

    current_line = ""

    words.each do |word|

      # We don't count highlighting (made with character '*')
      # for the length of the lines.
      length_of_word = (word.gsub('*', '')).length
      length_of_line = (current_line.gsub('*', '')).length

      if length_of_line + length_of_word > limit
        lines.push(current_line.delete_suffix(' '))

        current_line = ""
      end

      current_line << word

      current_line << ' '
    end

    lines.push(current_line.delete_suffix(' '))

    lines
  end

end
