class Indicator < ApplicationRecord
  belongs_to :indicator_category
  alias_attribute :category, :indicator_category

  has_many :facts, as: :factable, dependent: :destroy
  has_many :notes, dependent: :destroy
  accepts_nested_attributes_for :facts, :allow_destroy => true

  has_one_attached :icon

  scope :visible, -> { where visible:true }

  def selected_facts(countries = [], years = [])
    c = Country.where(id: countries)
    facts = Fact.where(factable: self)
    facts = facts.where(country: c) if !c.empty?
    facts = facts.where(year: years) if !years.empty?
    facts
  end

  def data(countries = [], years = [])
    c = Country.where(id: countries)
    facts = Fact.where(factable: self)
    p "1:" +  facts.count.to_s
    facts = facts.where(country: c) if !c.empty?
    p "2:" +  facts.count.to_s
    facts = facts.where(year: years) if !years.empty?
    p "3:" +  facts.count.to_s
    facts_to_grid(facts)
  end

  def facts_to_grid(facts)
    years = facts.distinct.pluck(:year).sort_by{|e| e }
    p ": years : " + years.to_s
    output_hash = []
    facts.distinct.pluck(:country_id).each do |c|
      country = Country.find c
      row_hash = {}
      years.each do |y|
        p "count facts: " + facts.where(country: country, year: y).count.to_s
        p "loko"
        # current_value = facts.where(country: country, year: y).first.value
        # row_hash.merge!({ y => current_value })
        current_value = facts.where(country: country, year: y).first
        p current_value
        row_hash.merge!({ y => current_value.value }) unless current_value.nil?
      end
      output_hash << {"label" => country.name, "data" => row_hash}
    end
    output_hash
  end
end
