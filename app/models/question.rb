class Question < ApplicationRecord
  belongs_to :question_category
  alias_attribute :category, :question_category

  has_many :facts, as: :factable, dependent: :destroy 
  accepts_nested_attributes_for :facts, :allow_destroy => true
  has_one_attached :background

  belongs_to :question_phrase
  alias_attribute :phrase, :question_phrase

  enum template: {  ninguno: -1,
                    viviendas: 0,
                    niños_y_niñas_empresas: 1,
                    privilegios: 2,
                    personas_ricas: 3,
                    maestros: 4,
                    medicos: 5,
                    violencia_machista: 6,
                    mujeres_ganan: 7,
                    hospital: 8,
                    respirador: 9,
                    canasta: 10,
                    tiempo: 11,
                    computadores: 12 }


  after_save :update_facts_images

  validate :question_text_base_must_be_valid

  def background_path
    ActiveStorage::Blob.service.send(:path_for, background.key)
  end

  def update_facts_images
    logger.debug "Attaching pngs to facts of question with ID #{self.id}"
    self.facts.each do |f|
      logger.debug "Attaching png to fact of ID #{f.id}"
      f.make_and_attach_png
      logger.debug "Attached png! To fact of ID #{f.id}"
    end
    logger.debug "Attached pngs! to facts of question with ID #{self.id}"
  end

  # This uses a helper method from Fact to check if the question text is too long
  # to be rendered properly
  def text_test_for_length(question_text)
    example_phrase = question_text.gsub '_value_', '800.000.000'
    example_phrase = example_phrase.gsub '_country_', 'ThisCountryNameIsHuge'

    logger.debug "Checking if #{question_text} is of valid length"

    Fact.is_phrase_an_appropriate_length(example_phrase)
  end

  def question_text_base_must_be_valid

    unless self.text.nil?
      text = self.text

      had_value_pattern = false
      had_country_pattern = false
      none_overlapped = false

      if text.include? '_value_'
        had_value_pattern = true
      end

      if text.include? '_country_'
        had_country_pattern = true
      end

      text = text.gsub '_value_', ''

      if text.include? '_country_'
        none_overlapped = true
      end

      unless had_value_pattern
        errors.add(:text, "must contain '_value_' pattern")
      end

      unless had_country_pattern
        errors.add(:text, "must contain '_country_' pattern")
      end

      if had_value_pattern and had_country_pattern
        errors.add(:text, "'_value_' and '_country_' must not overlap") unless none_overlapped

        errors.add(:text, 'Text is too long to be displayed properly in an image') unless text_test_for_length(self.text)
      end

    else
      errors.add(:text, "must not be nil")
    end

  end
end
