class QuestionPhrase < ApplicationRecord
  has_many :questions

  after_save :update_questions_images

  def update_questions_images
    logger.debug "Updating questions belonging to QuestionPhrase with ID #{self.id}"
    self.questions.each do |q|
      logger.debug "Updating question of ID #{q.id}"
      q.save
      logger.debug "Updated question of ID #{q.id}!"
    end
    logger.debug "Updated questions! Belonging to QuestionPhrase with ID #{self.id}"
  end
end
