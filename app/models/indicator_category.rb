class IndicatorCategory < ApplicationRecord
  has_many :indicators
  accepts_nested_attributes_for :indicators, :allow_destroy => true

  has_one_attached :icon
end
