class ContactMailer < ApplicationMailer
  default from: ENV["DEFAULT_FROM_EMAIL"]

  def contact_email
    @to = ENV["DEFAULT_TO_EMAIL"]
    @url  = 'http://example.com/login'
    # @params = params[:contact_params]
    @email = params[:email]
    @message = params[:message]
    mail(to: @to, subject: 'Contact Form dataigualdad.org', @email => params[:email], @message => params[:message])
  end
end
