class DataController < ApplicationController
  layout "layer2", only: [:questions, :dashboard]

  def indicator
    @indicator = Indicator.find(params[:indicator_id])
    @country_ids = params[:country_ids]
    @years = params[:years]
    respond_to do |format|
      format.json { render json: @indicator.data(@country_ids, @years), status: :ok }
    end

  end

  def dashboard
    id = data_params[:indicator_id] unless data_params.nil?
    @indicator = Indicator.find(id || Indicator.first.id)
    country_ids = data_params[:country_ids].reject!(&:empty?) unless data_params.nil? || data_params[:country_ids].nil?
    @country_ids = country_ids || []
    years = data_params[:years].reject!(&:empty?) unless data_params.nil? || data_params[:years].nil?
    @years = years || []
    @max = @indicator.data(@country_ids, @years).each.map { |country| country['data'].values.compact.max }.max unless data_params.nil?
    @min = @indicator.data(@country_ids, @years).each.map { |country| country['data'].values.compact.min }.min unless data_params.nil?
    @minmax = [@min, @max]
  end

  def questions

    country_id = question_params[:country_id]unless question_params.nil?
    @country_id = country_id || Country.first.id
    @facts = Fact.where(factable_type: 'Question', country_id: @country_id)
    @questions = Question.where facts: @facts
    @question = Question.all.first
    @question_categories = QuestionCategory.all

  end

  def fact
    # layout "layer2"

    fact_id = params[:fact_id]
    @fact = Fact.find fact_id
    render layout: "empty"
  end

  private
    def data_params
      params.require(:indicator).permit(:indicator_id, country_ids: [], years: []) unless params[:indicator].nil?
    end

    def question_params
      params.permit(:question_id, :country_id)
    end
end
