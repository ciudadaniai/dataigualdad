class PagesController < ApplicationController
    layout "layer2"

    def show
      if valid_page?
        render template: "pages/#{params[:page]}"
      else
        render file: "public/404.html", status: :not_found
      end
    end

    def contact
      p '**********************************************************************************************************************************'
      p '**********************************************************************************************************************************'
      p '**********************************************************************************************************************************'
      p '**********************************************************************************************************************************'
      p contact_params
      email = contact_params[:email]
      message = contact_params[:message]
      ContactMailer.with(email: email, message: message).contact_email.deliver_later
    end

    private
    def valid_page?
      File.exist?(Pathname.new(Rails.root + "app/views/pages/#{params[:page]}.html.erb"))
    end

    def contact_params
      params.permit(:email, :message, :authenticity_token, :fname, :lname, :website, :honeypot, :commit)
    end

  end
