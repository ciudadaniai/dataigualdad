# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or whered alongside the database with db:setup)..first_or_create
#
# Examples:
#
#   movies = Movie.where([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }]).first_or_create
#   Character.where(name: 'Luke', movie: movies.first).first_or_create
AdminUser.create(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

c1 = IndicatorCategory.where(name: 'Ingresos Tributarios', description: '').first_or_create
# Indicator.where(name: 'Ingresos tributarios totales como % del PIB', description: 'Se refiere al total ingresos tibutarios (del Gobierno central y general) ', category: c1).first_or_create
# Indicator.where(name: 'Impuestos directos como % del PIB', description: 'Es la suma de los ingresos tributarios que provienen de la renta, los beneficios y ganancias de capital de las personas físicas y jurídicas, los impuestos a la propiedad y otrosimpuestos directos', category: c1).first_or_create
# Indicator.where(name: 'Impuestos sobre la renta, los beneficios y ganancias del capital como % del PIB', description: 'Se refiere a los ingresos tributarios que provienen de la renta, los beneficios y ganancias de capital de las personas físicas y jurídicas', category: c1).first_or_create
Indicator.where(name: 'Impuestos sobre las personas físicas como % del PIB', description: 'Son los ingresos tributarios que provienen de la renta, los beneficios y ganancias de capital de las personas físicas', category: c1).first_or_create

c2 = IndicatorCategory.where(name: 'Gasto Social', description: '').first_or_create
Indicator.where(name: 'Gasto público social* como % del PIB (En porcentajes del producto interno bruto)', description: '', category: c2).first_or_create
Indicator.where(name: 'Gasto público en educación como % del PIB (En porcentajes del producto interno bruto)', description: '', category: c2).first_or_create
# Indicator.where(name: 'Gasto público en salud como % del PIB (En porcentajes del producto interno bruto)', description: '', category: c2).first_or_create
# Indicator.where(name: 'Gasto público en protección social como % del PIB (En porcentajes del producto interno bruto)', description: '', category: c2).first_or_create

c3 = IndicatorCategory.where(name: 'Democracia', description: '').first_or_create
Indicator.where(name: 'Índice de apoyo a la democracia', description: '', category: c3).first_or_create
# Indicator.where(name: 'Porcentaje de personas que apoyan a la democracia (En porcentajes)', description: '', category: c3).first_or_create
# Indicator.where(name: 'Porcentaje de la población que cree que se gobierna para unos cuantos grupos poderosos en su propio beneficio (en porcentajes)', description: '', category: c3).first_or_create
Indicator.where(name: 'Porcentaje de la población que aprueba la gestión del Gobierno (En porcentajes)', description: '', category: c3).first_or_create

c4 = IndicatorCategory.where(name: 'Corrupción', description: '').first_or_create
Indicator.where(name: 'Percepción de corrupción de funcionarios públicos (En porcentajes)', description: '', category: c4).first_or_create
Indicator.where(name: 'Porcentaje de la población que opina que vale la pena tolerar la corrupción a cambio de la solución de los problemas (En porcentajes)', description: '', category: c4).first_or_create

c5 = IndicatorCategory.where(name: 'Salud', description: '').first_or_create
Indicator.where(name: 'Incidencia de tuberculosis (por cada 100,000 habitantes)', description: '', category: c5).first_or_create
Indicator.where(name: 'Tasa de deteccion de tuberculosis (todas las formas)', description: '', category: c5).first_or_create
# Indicator.where(name: 'Tasa de mortalidad materna (por cada 100.000 nacidos vivos)', description: '', category: c5).first_or_create
# Indicator.where(name: 'Tasa de mortalidad, menores de 5 años (por cada 1.000)', description: '', category: c5).first_or_create
# Indicator.where(name: 'Salario mensual de un médico (En moneda nacional)', description: '', category: c5).first_or_create

c6 = IndicatorCategory.where(name: 'Educación', description: '').first_or_create
# Indicator.where(name: 'Tasa neta de matrícula, primaria, ambos sexos (%)', description: '', category: c6).first_or_create
# Indicator.where(name: 'Tasa neta de matrícula, secundaria, ambos sexos (%)', description: '', category: c6).first_or_create
Indicator.where(name: 'Número de niños que no asisten a la escuela, nivel primario, ambos sexos', description: '', category: c6).first_or_create
Indicator.where(name: 'Salario mensual de un maestro de escuela (En moneda nacional)', description: '', category: c6).first_or_create

c7 = IndicatorCategory.where(name: 'Derechos y fiscalidad', description: '').first_or_create
Indicator.where(name: 'Índice de presupuesto abierto (puntaje sobre un total de 100).', description: '', category: c7).first_or_create

c8 = IndicatorCategory.where(name: 'Otros', description: '').first_or_create
Indicator.where(name: 'Precio de la solución de vivienda más barata del sector  privado, 2010 (ciudades principales)', description: '', category: c8).first_or_create

ar = Country.where(short_name: 'Argentina',name: 'Argentina').first_or_create
bo = Country.where(short_name: 'Bolívia',name: 'Bolivia (Estado Plurinacional de)').first_or_create
Country.where(short_name: 'Brasil',name: 'Brasil').first_or_create
ch = Country.where(short_name: 'Chile',name: 'Chile').first_or_create
Country.where(short_name: 'Colombia',name: 'Colombia').first_or_create
Country.where(short_name: 'Costa Rica',name: 'Costa Rica').first_or_create
Country.where(short_name: 'Cuba',name: 'Cuba').first_or_create
Country.where(short_name: 'Ecuador',name: 'Ecuador').first_or_create
Country.where(short_name: 'El Salvador',name: 'El Salvador').first_or_create
Country.where(short_name: 'Guatemala',name: 'Guatemala').first_or_create
Country.where(short_name: 'Haití',name: 'Haití').first_or_create
Country.where(short_name: 'Honduras',name: 'Honduras').first_or_create
Country.where(short_name: 'México',name: 'México').first_or_create
Country.where(short_name: 'Nicaragua',name: 'Nicaragua').first_or_create
Country.where(short_name: 'Panamá',name: 'Panamá').first_or_create
Country.where(short_name: 'Paraguay',name: 'Paraguay').first_or_create
Country.where(short_name: 'Perú',name: 'Perú').first_or_create
Country.where(short_name: 'R.Dominicana',name: 'República Dominicana').first_or_create
Country.where(short_name: 'Uruguay',name: 'Uruguay').first_or_create
Country.where(short_name: 'Venezuela',name: 'Venezuela (República Bolivariana de)').first_or_create
# Country.where(short_name: 'ALC',name: 'América Latina y el Caribe').first_or_create
Country.where(short_name: 'Media ALC',name: 'América Latina y el Caribe').first_or_create

Indicator.all.each do |i|
  p "faking data for indicator:" + i.name.to_s
  Country.all.each_with_index do |c, ic|
    years = 2010..2018
    years.each do |y|
      Fact.create(factable: i, country: c, year: y, value: 1.0 + (ic.to_f/10) + (y.to_f/20))
    end
  end
end

QuestionCategory.create(name: 'Impuestos y privilegios', short_name: "impypriv", message: "Los privilegios tributarios a personas y empresas afectan directamente a la igualdad en #AméricaLatina. Conoce más #DatosParaActuar en http://dataigualdad.org. @ciudadaniai @oxfam_es")
salud = QuestionCategory.create(name: "Salud", short_name: "salud", message: "La salud es un derecho, pero nuestras democracias no siempre la garantizan. Conoce la situación de desigualdad de este y otros países de #AméricaLatina a través de los #DatosParaActuar de http://dataigualdad.org @ciudadaniai @oxfam_es")
QuestionCategory.create(name: "Educación", short_name: "educ", message: "La educación es clave para combatir la desigualdad. Conoce más #DatosParaActuar sobre este y otros temas en http://dataigualdad.org @ciudadaniai @oxfam_es")
ing = QuestionCategory.create(name: 'Gasto social', short_name: "gasto", message: "¿Cuánto invierte tu gobierno en las áreas que más afectan la vida de la ciudadanía? Conoce más #DatosParaActuar sobre este y otros países en http://dataigualdad.org. @ciudadaniai @oxfam_es")
QuestionCategory.create(name: "Democracia", short_name: "demo", message: "La desigualdad afecta la calidad de las democracias de #AméricaLatina. Entérate de otros #DatosParaActuar sobre los países de nuestra región en http://dataigualdad.org @ciudadaniai @oxfam_es")
QuestionCategory.create(name: "Género", short_name: "genereo", message: "Si no conocemos cómo la desigualdad impacta la vida de las personas, no podemos combatirla. Conoce más #DatosParaActuar sobre toda #AméricaLatina en http://dataigualdad.org @ciudadaniai @oxfam_es")
QuestionCategory.create(name: "Corrupción", short_name: "corrupcion", message: "La corrupción quita el poder a la ciudadanía para traspasárselo a unos pocos. Conoce más #DatosParaActuar sobre #AméricaLatina en http://dataigualdad.org @ciudadaniai @oxfam_es")




p "Question Categories done!"

qp0 = QuestionPhrase.create(phrase: 'La desigualdad atenta contra nuestro bienestar')
qp1 = QuestionPhrase.create(phrase: 'Infórmate sobre cómo se distribuyen los recursos de todas y todos en: *DataIgualdad.org*')
qp2 = QuestionPhrase.create(phrase: '¡Basta de desigualdades!')
qp3 = QuestionPhrase.create(phrase: 'Infórmate cómo los gobiernos usan nuestro dinero en *DataIgualdad.org*')
qp4 = QuestionPhrase.create(phrase: 'Para cambiar las cosas, necesitamos información')
qp5 = QuestionPhrase.create(phrase: 'Infórmate sobre cómo la desigualdad está afectando a la ciudadanía de tu país en: *DataIgualdad.org*')
qp6 = QuestionPhrase.create(phrase: 'La desigualdad atenta contra la democracia')
qp7 = QuestionPhrase.create(phrase: '¿Te parece justo? Infórmate en dataigualdad.org')

p "Question Phrases done!"


q1 = Question.create( text: "En _country_, un _value_% de los ingresos tributarios proviene de impuestos directos (es decir, hacen pagar más a los que más tienen).",
                      concept: "Los impuestos a la renta, la riqueza o la propiedad hacen pagar más a los que más tienen ¿Qué parte de los ingresos tributarios en tu país proviene de este tipo de impuestos?",
                      visible: true,
                      question_category: salud,
                      question_phrase: qp2,
                      template: :privilegios)

p "First Question done!"

Fact.skip_callback(:save, :before, :make_one_png)

Fact.create(factable: q1, country: ar, year: 2012, value: 21 )
Fact.create(factable: q1, country: ch, year: 2013, value: 22.5 )
Fact.create(factable: q1, country: bo, year: 2013, value: 22.324 )

p "First Facts done!"

q2 = Question.create( text: "En _country_, un _value_% de los ingresos tributarios proviene de impuestos directos (es decir, hacen pagar más a los que más tienen). Este texto es extra largo.",
                      concept: "Los impuestos al consumo no diferencian suficientemente entre las personas más ricas o más pobres y por lo tanto pueden profundizar la desigualdad. ¿Qué parte de los ingresos tributarios en tu país proviene de impuestos al consumo?",
                      visible: true,
                      question_category: salud,
                      question_phrase: qp1,
                      template: :personas_ricas)

p "Second Question done!"

Fact.create(factable: q2, country: ar, year: 2012, value: 8800 )
p "Second Facts done!"

q3 = Question.create( text: "En _country_, un _value_% de los ingresos tributarios proviene de impuestos directos (es decir, hacen pagar más a los que más tienen). Este texto es ridículamente largo, wooow wooooooow.",
                      concept: "Los impuestos al consumo no diferencian suficientemente entre las personas más ricas o más pobres y por lo tanto pueden profundizar la desigualdad. ¿Qué parte de los ingresos tributarios en tu país proviene de impuestos al consumo?",
                      visible: true,
                      question_category: salud,
                      question_phrase: qp1,
                      template: :mujeres_ganan)

p "Third Question done!"

Fact.create(factable: q3, country: ar, year: 2012, value: 8800 )
p "Third Facts done!"

Fact.set_callback(:save, :before, :make_one_png)

# Question.all.map{|q| q.save}
