class CreateFacts < ActiveRecord::Migration[6.0]
  def change
    create_table :facts, id: :uuid do |t|
      t.references :indicator, null: false, foreign_key: true, type: :uuid
      t.references :country, null: false, foreign_key: true, type: :uuid
      t.integer :year
      t.float :value

      t.timestamps
    end
  end
end
