class AddQuestionPhraseRefToQuestions < ActiveRecord::Migration[6.0]
  def change
    add_column :questions, :question_phrase_id, :uuid
    add_index :questions, :question_phrase_id
    add_foreign_key :questions, :question_phrases
  end
end
