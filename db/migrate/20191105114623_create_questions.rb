class CreateQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :questions, id: :uuid do |t|
      t.string :text
      t.string :measure
      t.boolean :visible
      t.references :indicator, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
