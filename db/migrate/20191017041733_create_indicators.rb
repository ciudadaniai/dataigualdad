class CreateIndicators < ActiveRecord::Migration[6.0]
  def change
    create_table :indicators, id: :uuid do |t|
      t.string :name
      t.string :description
      t.string :measure
      t.boolean :visible, null: false, default: false
      t.integer :order
      t.references :indicator_category, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
