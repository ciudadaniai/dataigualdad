class AddImageTemplateToQuestions < ActiveRecord::Migration[6.0]
  def change
    add_column :questions, :template, :integer, default: -1
  end
end
