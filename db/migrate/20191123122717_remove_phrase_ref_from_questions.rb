class RemovePhraseRefFromQuestions < ActiveRecord::Migration[6.0]
  def change
    remove_index :questions, :question_phrase_id if index_exists?(:questions, :question_phrase_id)
    remove_foreign_key :questions, :question_phrases
    remove_column :questions, :question_phrase_id
  end
end
