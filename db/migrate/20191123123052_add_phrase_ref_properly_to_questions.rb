class AddPhraseRefProperlyToQuestions < ActiveRecord::Migration[6.0]
  def change
    add_reference :questions, :question_phrase, polymorphic: false, index: true, type: :uuid

  end
end
