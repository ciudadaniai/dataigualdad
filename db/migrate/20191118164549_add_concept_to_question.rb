class AddConceptToQuestion < ActiveRecord::Migration[6.0]
  def change
    add_column :questions, :concept, :string
  end
end
