class AddPolymorphicReferenceToFacts < ActiveRecord::Migration[6.0]
  def change

    remove_reference :facts, :indicator, index: true, foreign_key: true
    add_reference :facts, :factable, polymorphic: true, index: true, type: :uuid

  end
end
