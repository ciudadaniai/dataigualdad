class AddQuestionCategoryRefToQuestion < ActiveRecord::Migration[6.0]
  def change
    add_reference :questions, :question_category, polymorphic: false, index: true, type: :uuid
  end
end
