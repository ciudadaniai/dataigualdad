class RemoveIndicatorFromQuestion < ActiveRecord::Migration[6.0]
  def change
    remove_reference :questions, :indicator, index: true, foreign_key: true
  end
end
