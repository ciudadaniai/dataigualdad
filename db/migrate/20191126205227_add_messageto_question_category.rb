class AddMessagetoQuestionCategory < ActiveRecord::Migration[6.0]
  def change
      add_column :question_categories, :message, :string
  end
end
