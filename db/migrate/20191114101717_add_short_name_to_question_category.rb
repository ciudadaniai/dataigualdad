class AddShortNameToQuestionCategory < ActiveRecord::Migration[6.0]
  def change
    add_column :question_categories, :short_name, :string
  end
end
