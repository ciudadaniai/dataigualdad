class CreateQuestionPhrases < ActiveRecord::Migration[6.0]
  def change
    create_table :question_phrases, id: :uuid do |t|
      t.string :phrase

      t.timestamps
    end
  end
end
