require 'csv'
require 'roo'
require 'action_view'
include ActionView::Helpers::SanitizeHelper

namespace :import_data do
  task :indicators => :environment do

    p 'Indicator Data Load starting...'


    xlsx = Roo::Excelx.new("db/20200109_Indicadores_v2.0.xlsx")
    Indicator.destroy_all

    # p xlsx.info
    sheet_counter = 0
    xlsx.sheets.each do |sheet|
      sheet == 'Preguntas país' ? exit :
      # p sheet.inspect
      str = sheet
      re = Regexp.new("^\\d+").freeze
      # re = Regexp.new("^[1]").freeze
      if re.match str
        sheet_counter = sheet_counter + 1
        # p "match: " + str + " num: " + sheet_counter.to_s
        csv = xlsx.sheet(sheet).to_csv
        file_name = 'tmp/' + sheet + '.out'
        file_log = 'tmp/' + sheet + '.log'
        p file_name
        p "indicators: " + Indicator.count.to_s + " , facts: " + Fact.count.to_s
        open( file_name, 'w') do |f|
          f << csv
        end

        file = file_name

        counter = 0
        indicator = nil
        CSV.foreach(file, headers: true) do |row|
          # p "All headers : " + row.headers.count.to_s
          # p row
          # p row
          fact_headers = row.headers
          fact_headers.shift
          # p "All headers after shift : " + fact_headers.count.to_s
          # p fact_headers.inspect
          clean_name = strip_tags(row.headers[0]).gsub(/^\d+/,'').strip
          indicator = Indicator.find_by(name: clean_name)
          if counter == 0
            category = IndicatorCategory.find_or_create_by(name: row["Categoría:"])
            # p category.errors
            # p category.name
            # p "category: " + category.name

            p clean_name
            indicator = Indicator.find_or_create_by(name: clean_name, visible: false, indicator_category: category)
            counter = counter + 1

          else
            p "clean_name: " + clean_name
            indicator = Indicator.find_by(name: clean_name)

          end
          unless row[0].nil?
            p row
            country_name = row[0].gsub('*','').gsub('/','').gsub(/\d+/,'').strip
            if country_name == 'Media ALC'
              country_name = 'América Latina y el Caribe'
            elsif country_name == 'América Latina y el Caribe ( países)'
              country_name = 'América Latina y el Caribe'
            end
            p country_name
            p row[0]
            country = Country.find_by(name: country_name)
            # p 'country'
            # p "columns to import: " + fact_headers.count.to_s
            fact_headers.delete("Categoría:")
            fact_headers.delete("Categoría")
            # p "columns to import: " + fact_headers.count.to_s
            open( file_log, 'w') do |f|
              f << file_name + "\n" + "\n"
              f << country_name + "\n"
              f << "columns to import: " + fact_headers.count.to_s + "\n"
              fact_headers.each do |header|
                f <<  "header: " + header.to_s + "\n"
                f <<  "row[header]: " + row[header].to_s + " " + "\n"
                unless row[header].nil?
                  # value = row[header].to_s.gsub '.', ','
                  value = row[header]
                  f <<  "value: " + value.to_s + "\n"
                  # value = value.gsub ',', '.'
                    # p "country row: " + row[0]
                    # p "country: " + (country.name || "na") +  " : " + row[0]
                    # p row
                    f <<  row.to_s + "\n"
                  fact = Fact.create(factable_type: "Indicator", factable_id: indicator.id, country: country, year: header, value: value)
                  if fact.errors.messages.count > 0
                    p fact.errors.messages.to_s
                    p "country row: " + row[0]
                    # p "country: " + (country.name || "na") +  " : " + row[0]
                    p " year: " + header
                    p " value:" + value
                  else
                    f <<  "id: " + fact.id.to_s + "\n"
                  end
                end
              end
              f << "indicator fact count: " + indicator.facts.count.to_s + "\n"

            end
            # p indicator.inspect
            # p indicator.errors
          end
        end
        # read sources
        array = File.read(file).split("\n")
        unless array.nil?
          array = File.read(file).split("\n")[23,array.size-1]
          unless array.nil?
            array.each_with_index do |line, index|
              unless line.gsub(',','').size == 0
                # p "line:" + line.split(',')[0].to_s
                # p line
                Note.create(indicator: indicator, text: strip_tags(line).gsub(',', ''))
              end
            end
          end
        end


        # exit

        # File.delete(file_name) if File.exists? file_name
      end
    end



    # file = "db/tab_example.csv"
    #
    # Indicator.destroy_all
    #
    # CSV.foreach(file, headers: true) do |row|
    #   # p "All headers : " + row.headers.count.to_s
    #   # p row.headers.inspect
    #   fact_headers = row.headers
    #   fact_headers.shift
    #   # p "All headers after shift : " + fact_headers.count.to_s
    #   # p fact_headers.inspect
    #   indicator = Indicator.find_or_create_by(name: row.headers[0], visible: true, indicator_category: IndicatorCategory.first)
    #   country = Country.find_by(name: row[0])
    #   fact_headers.each do |header|
    #     # p "header: " + header.to_s
    #     # p "row[header]: ." + row[header].to_s + "."
    #     unless row[header].nil?
    #       value = row[header].gsub '.', ''
    #       value = value.gsub ',', '.'
    #       fact = Fact.create(factable_type: "Indicator", factable_id: indicator.id, country: country, year: header, value: value)
    #       # p fact.inspect
    #     end
    #   end
    #   # p indicator.inspect
    #   # p indicator.errors
    # end

  end
end
