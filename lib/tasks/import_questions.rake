require 'csv'
namespace :import_data do
  task :questions => :environment do

    p 'deleting data '
    Fact.destroy_all
    Question.destroy_all
    # QuestionCategory.destroy_all
    # Indicator.destroy_all
    p "dataigualdad questions csv import"

    file = "db/final_import.csv"
    q = Question.first
    CSV.foreach(file, headers: true) do |row|
     row_hash = row.to_hash
     # p row.headers
     if row_hash["country"] == "Paises"
       text = row_hash["question"]
       concept = row_hash["concept"]
       cat= QuestionCategory.find_or_create_by(name: row_hash["category"])

       template_map = { "Impuestos y privilegios" => 2 }
       template_map["Democracia"] = 1
       template_map["Salud"] = 5
       template_map["Educación"] = 4
       template_map["Gasto social"] = 0
       template_map["Género"] = 7
       template_map["Corrupción"] = 3
       p "text: " + text
       p "concept: " + concept
       p "cat.name: " + row_hash["category"]
       p "cat.name: " + cat.name
       template = template_map[cat.name] || 6
       p "template: " + template.to_s
       p "QuestionPhrase.first: " + QuestionPhrase.first.to_s
       q = Question.find_or_create_by(text: text, concept: concept, question_category: cat || QuestionCategory.first, question_phrase: QuestionPhrase.first, template: template.to_i)
       p q.text

       p "question count: " + Question.count.to_s
     else
       country = Country.find_by(name: row_hash["country"])
       # p 'found country: ' + country.name

       value = row_hash["question"].gsub ',', ''
       value = value.gsub '.', ','
       p  "pues si pasamos "

       Fact.find_or_create_by(
         factable: q,
         value: value,
         year: row_hash["year"],
         country: country
         ) unless row_hash["question"] == '…'

       p  "por aqui ------------------------------------------------------no ----------- pasamos "
      p  "." + row_hash["question"]
      p "fact count: " + Fact.count.to_s
     end

     # (1..24).each do |i|
     #   p i
     #   p row.headers[i*3 + 0]
     #   p row.headers[i*3 + 1]
     #   p row.headers[i*3 + 2]
     #   p 'value: '  + row_hash[i*3 + 1].to_s
     #   p 'year: '  + row_hash[i*3 + 3].to_s
     #   p 'pos: '  + row_hash[i*3 + 2].to_s
     #
     # end
     # p row_hash
    end
    # p "question count: " + Question.count.to_s
    Question.all.map{|q| q.save}
  end
end
