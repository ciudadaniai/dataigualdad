[![pipeline status](https://gitlab.com/ciudadaniai/dataigualdad/badges/development/pipeline.svg)](https://gitlab.com/ciudadaniai/dataigualdad/commits/development)

Dataigualdad

https://www.draw.io/#G1IdifjLwmb3pd6W-k_Ickpk-yreGc97IG

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

ruby on rails 6.0.0
postgresql
devise
active_admin
using uuid for id fields


https://github.com/activeadmin/activeadmin/issues/5258
http://www.abarrak.com/2019/05/24/uuid-primary-key-for-active-record-models

this app is copied from rails-6-activeadmin-base and renamed as explained in the following link:
https://stackoverflow.com/questions/42326432/how-to-rename-a-rails-5-application

to use activestorage on activeadmin
https://medium.com/@maris.cilitis/using-ruby-on-rails-active-storage-image-uploads-for-active-admin-backed-resources-5638a9ca0b46


https://yarnpkg.com/en/docs/install#debian-stable
yarn is a new requirement for js libs management

https://tecadmin.net/install-postgresql-on-debian-10-buster/
to run the app on a debian system libpq-dev package must be installed
then:
- CREATE USER dataigualdad WITH SUPERUSER;
- ALTER ROLE dataigualdad WITH PASSWORD 'wordpass';
- CREATE DATABASE dataigualdad_dev WITH OWNER dataigualdad;

events and visits on site tracking
https://github.com/ankane/ahoy

to better understand the active_admin dsl this may be usefull
https://github.com/activeadmin/inherited_resources
http://staal.io/blog/2013/02/26/mastering-activeadmin/

https://stackoverflow.com/questions/3975499/convert-svg-to-image-jpeg-png-etc-in-the-browser

required libs for image processing:
* rmagic needs imagemagick and libmagick in arch
* inkscape
* libmagickwand-dev
